/**
 * @format
 */
import React, {Suspense} from "react";
import { AppRegistry, Text } from "react-native";
import App from "./src/App";
import { name as appName } from "./app.json";
import { Provider } from "react-redux";
import useStore from "./src/store";
import { PersistGate } from "redux-persist/integration/react";

const ReduxProvider = () => {
  const { store, persistor } = useStore();

  return (
    <Suspense fallback={<Text>Cargando...!!</Text>}>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <App />
        </PersistGate>
      </Provider>
    </Suspense>
  );
};

AppRegistry.registerComponent(appName, () => ReduxProvider);
