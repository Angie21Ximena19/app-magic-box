import useCreateSelector from "../createSelector";

const useSaludoSelectors = () => {
  const { createSelector } = useCreateSelector();

  const textSelector = createSelector(
    (state: any) => state.saludo,
    (saludo: any) => saludo.saludo
  );

  return {
    textSelector,
  };
};

export default useSaludoSelectors;
