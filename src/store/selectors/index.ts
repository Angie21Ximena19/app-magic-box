import useSaludoSelectors from "./saludo";

const useSelectors = () => {
  return {
    useSaludoSelectors,
  };
};

export default useSelectors;
