import { combineReducers } from "@reduxjs/toolkit";
import useSaludoReducers from "./saludo";

const useReducers = () => {
  const { saludo } = useSaludoReducers();

  return combineReducers({
    saludo,
  });
};

export default useReducers;
