import useCreateReducer from "../createReducer";
import { act } from "react-test-renderer";

const useSaludoReducers = () => {
  const { createReducer } = useCreateReducer();

  const saludo = createReducer({
    saludo: ""
  }, {
    ["SET_SALUDO"](state: any, action: any) {
      return {
        ...state,
        saludo: action.payload
      };
    }
  });

  return {
    saludo,
  };
};

export default useSaludoReducers;
