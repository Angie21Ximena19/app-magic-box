import useReducers from "./reducers";
import thunk from "redux-thunk";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer, persistStore } from "redux-persist";
import { applyMiddleware, compose, createStore } from "@reduxjs/toolkit";

const useStore = () => {
  let middleware = [thunk];
  const rootReducers = useReducers(),
    initialState = {},
    persistConfig = {
      key: "root",
      storage: AsyncStorage,
      whitelist: ['saludo']
    },
    persistReduce = persistReducer(persistConfig, rootReducers);

  if (process.env.NODE_ENV === "development") {
    const reduxImmutableStateInvariant = require("redux-immutable-state-invariant").default();
    middleware = [...middleware, reduxImmutableStateInvariant];
  }

  const composeEnhancers = compose;

  let store = createStore(
    persistReduce,
    initialState,
    composeEnhancers(applyMiddleware(...middleware))
  );

  let persistor = persistStore(store);

  return {
    store,
    persistor,
  };
};

export default useStore;
