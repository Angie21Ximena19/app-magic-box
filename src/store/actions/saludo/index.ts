const useSaludoActions = () => {
  const actSaludo = (text: string) => async (dispatch: any) => {
    try {
      console.log(text);
      dispatch({
        type: "SET_SALUDO",
        payload: text,
      });
    } catch (e) {
      console.log(e);
    }
  };

  return {
    actSaludo,
  };
};

export default useSaludoActions;
