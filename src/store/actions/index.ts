import { useDispatch } from "react-redux";
import useSaludoActions from "./saludo";

const useActions = () => {
  const dispatch = useDispatch();
  return {
    dispatch,
    useSaludoActions,
  };
};

export default useActions;
