import React from "react";
import { Button, Text } from "react-native";
import useActions from "./store/actions";
import useSelectors from "./store/selectors";
import { useSelector } from "react-redux";

const App = () => {
  // Actions
  const { dispatch, useSaludoActions } = useActions();
  const { actSaludo } = useSaludoActions();

  // Selectors
  const { useSaludoSelectors } = useSelectors();
  const { textSelector } = useSaludoSelectors();
  const text = useSelector(textSelector);

  return (
    <React.Fragment>
      <Text>Hola {text}</Text>
      <Button title="saludo" onPress={() => dispatch(actSaludo('Anyi'))}/>
    </React.Fragment>

  );
};

export default App;
